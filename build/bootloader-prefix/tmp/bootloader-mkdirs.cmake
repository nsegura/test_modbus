# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "D:/Users/nesg1/esp/esp-idf/components/bootloader/subproject"
  "D:/proyectos_ESP32/modbus_test/modbus_test/build/bootloader"
  "D:/proyectos_ESP32/modbus_test/modbus_test/build/bootloader-prefix"
  "D:/proyectos_ESP32/modbus_test/modbus_test/build/bootloader-prefix/tmp"
  "D:/proyectos_ESP32/modbus_test/modbus_test/build/bootloader-prefix/src/bootloader-stamp"
  "D:/proyectos_ESP32/modbus_test/modbus_test/build/bootloader-prefix/src"
  "D:/proyectos_ESP32/modbus_test/modbus_test/build/bootloader-prefix/src/bootloader-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "D:/proyectos_ESP32/modbus_test/modbus_test/build/bootloader-prefix/src/bootloader-stamp/${subDir}")
endforeach()
if(cfgdir)
  file(MAKE_DIRECTORY "D:/proyectos_ESP32/modbus_test/modbus_test/build/bootloader-prefix/src/bootloader-stamp${cfgdir}") # cfgdir has leading slash
endif()
